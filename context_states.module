<?php
/**
 * @file
 * Module for defining general states for usage in context
 * This module provides an admin page /admin/structure/states/define for
 * administrators to add states and a separate admin page
 * /admin/structure/states for editors to select the current state of the site.
 */

/**
 * Implements hook_menu().
 */
function context_states_menu() {
  $items = array();

  $items['admin/structure/states'] = array(
    'title' => 'States',
    'description' => 'States UI',
    'page callback' => 'drupal_get_form',
    'file' => 'context_states.admin.inc',
    'file path' => drupal_get_path('module', 'context_states'),
    'page arguments' => array('context_states_form'),
    'access arguments' => array('edit states'),
  );

  $items['admin/structure/states/edit'] = array(
    'title' => 'Edit States',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -1,
  );

  $items['admin/structure/states/define'] = array(
    'title' => 'Define States',
    'description' => 'Define States as an Administrator',
    'page callback' => 'drupal_get_form',
    'file' => 'context_states.admin.inc',
    'file path' => drupal_get_path('module', 'context_states'),
    'page arguments' => array('context_states_define_states_form'),
    'access arguments' => array('define states'),
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function context_states_permission() {
  return array(
    'define states' => array(
      'title' => t('Define States'),
      'description' => t('Define States as an Administrator'),
    ),
    'edit states' => array(
      'title' => t('Edit States'),
      'description' => t('Toggle the state(s) through the UI'),
    ),
  );
}

/**
 * STATES CONTEXT (see API.txt in context module for more info)
 */

/**
 * Implements hook_context_plugins().
 */
function context_states_context_plugins() {
  $plugins = array();
  $plugins['context_states_context_condition_states'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'context_states'),
      'file' => 'context_states_context_condition_states.inc',
      'class' => 'context_states_context_condition_states',
      'parent' => 'context_condition',
    ),
  );
  return $plugins;
}

/**
 * Implements hook_context_registry().
 */
function context_states_context_registry() {
  return array(
    'conditions' => array(
      'context_states' => array(
        'title' => t('States'),
        'description' => t('Apply state(s)'),
        'plugin' => 'context_states_context_condition_states',
      ),
    ),
  );
}
/**
 * Implements hook_init().
 */
function context_states_init() {
  if ($plugin = context_get_plugin('condition', 'context_states')) {
    $states = explode("\n", variable_get('context_states_defined_states', ''));
    foreach ($states as $key => &$value) {
      $state = variable_get('context_states_states_' . $key);
      if ($state == 1) {
        $plugin->execute($key);
      }
    }
  }
}
